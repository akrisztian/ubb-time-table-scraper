import { Controller, Get, Param } from '@nestjs/common';
import { StudentService } from '../service/student.service';

@Controller('students')
export class StudentController {
  constructor(private studentService: StudentService) {}

  @Get(':year/:semester/:section/:group')
  getStudentGroupTable(
    @Param('year') year: number,
    @Param('semester') semester: number,
    @Param('section') section: string,
    @Param('group') group: number,
  ) {
    return this.studentService.findSectionGroupTable(
      year,
      semester,
      section,
      group,
    );
  }

  @Get(':year/:semester/:section')
  getStudentSectionTables(
    @Param('year') year: number,
    @Param('semester') semester: number,
    @Param('section') section: string,
  ) {
    return this.studentService.findSectionTables(year, semester, section);
  }
}
