import { IsString } from 'class-validator';

export class StudentClassDto {
  @IsString()
  day: string;
  @IsString()
  time: string;
  @IsString()
  frequency: string;
  @IsString()
  room: string;
  @IsString()
  group: string;
  @IsString()
  type: string;
  @IsString()
  course: string;
}
