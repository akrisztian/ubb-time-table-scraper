import { StudentClassDto } from './student-class.dto';

export class StudentTableDto {
  classes: StudentClassDto[];
}
