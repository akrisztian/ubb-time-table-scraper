import { HttpService, Injectable } from '@nestjs/common';
import { load } from 'cheerio';
import { Tabletojson as tableConverter } from 'tabletojson';
import { map } from 'rxjs/operators';
import { StudentTableDto } from '../dto/student-table.dto';
import { Observable } from 'rxjs';

@Injectable()
export class StudentService {
  readonly baseUrl = 'https://www.cs.ubbcluj.ro/files/orar';

  constructor(private http: HttpService) {}

  findSectionTables(
    year: number,
    semester: number,
    section: string,
  ): Observable<StudentTableDto[]> {
    const url = `${this.baseUrl}/${year}-${semester}/tabelar/${section}.html`;

    return this.http.get<string>(url).pipe(
      map((html) => {
        const selector = load(html.data);
        const tables = selector('body').find('table');
        const studentTables: StudentTableDto[] = [];

        for (let i = 0; i < tables.length; ++i) {
          studentTables.push({ classes: [] });
          tableConverter
            .convert(tables[i] as unknown as string)[0]
            .forEach((elem) => {
              studentTables[studentTables.length - 1].classes.push({
                day: elem.Ziua,
                time: elem.Orele,
                frequency: elem.Frecventa,
                room: elem.Sala,
                group: elem.Formatia,
                type: elem.Tipul,
                course: elem.Disciplina,
              });
            });
        }
        return studentTables;
      }),
    );
  }

  findSectionGroupTable(
    year: number,
    semester: number,
    section: string,
    group: number,
  ): Observable<StudentTableDto> {
    return this.findSectionTables(year, semester, section).pipe(
      map((tables) => tables[(group % 10) - 1]),
    );
  }
}
