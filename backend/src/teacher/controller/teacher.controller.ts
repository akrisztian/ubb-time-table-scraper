import { Controller, Get, Param } from '@nestjs/common';
import { Observable } from 'rxjs';
import { TeacherDto } from '../dto/teacher.dto';
import { TeacherService } from '../service/teacher.service';

@Controller('teachers')
export class TeacherController {
  constructor(private teacherService: TeacherService) {}

  @Get(':year/:semester')
  getTeacherInfoByYearAndSemester(
    @Param('year') year: number,
    @Param('semester') semester: number,
  ): Observable<TeacherDto[]> {
    return this.teacherService.findAllByYearAndSemester(year, semester);
  }

  @Get(':year/:semester/:teacherId')
  getTeacherTable(
    @Param('year') year: number,
    @Param('semester') semester: number,
    @Param('teacherId') teacherId: string,
  ) {
    return this.teacherService.findTeacherTable(year, semester, teacherId);
  }
}
