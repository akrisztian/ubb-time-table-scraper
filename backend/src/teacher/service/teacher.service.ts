import { HttpService, Injectable } from '@nestjs/common';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { TeacherDto } from '../dto/teacher.dto';
import { load } from 'cheerio';
import { Tabletojson as tableConverter } from 'tabletojson';
import { TeacherTableDto } from '../dto/teacher-table.dto';

@Injectable()
export class TeacherService {
  readonly baseUrl = 'https://www.cs.ubbcluj.ro/files/orar';

  constructor(private http: HttpService) {}

  findAllByYearAndSemester(
    year: number,
    semester: number,
  ): Observable<TeacherDto[]> {
    const url = `${this.baseUrl}/${year}-${semester}/cadre/index.html`;

    return this.http.get<string>(url).pipe(
      map((html) => {
        const teachers: TeacherDto[] = [];
        const selector = load(html.data);
        selector('table')
          .find('td')
          .each((index, elem) => {
            teachers.push({
              name: selector(elem).text(),
              id: selector(elem).find('a').attr('href').split('.')[0],
            });
          });
        return teachers;
      }),
    );
  }

  findTeacherTable(
    year: number,
    semester: number,
    id: string,
  ): Observable<TeacherTableDto> {
    const url = `${this.baseUrl}/${year}-${semester}/cadre/${id}.html`;

    return this.http.get<string>(url).pipe(
      map((html) => {
        const selector = load(html.data);
        const tables = selector('body').find('table');
        const table: TeacherTableDto = { classes: [] };

        tableConverter
          .convert(tables[0] as unknown as string)[0]
          .forEach((elem) => {
            table.classes.push({
              day: elem.Ziua,
              time: elem.Orele,
              frequency: elem.Frecventa,
              room: elem.Sala,
              year: elem.Anul,
              group: elem.Formatia,
              type: elem.Tipul,
              course: elem.Disciplina,
            });
          });
        return table;
      }),
    );
  }
}
