import { HttpModule, Module } from '@nestjs/common';
import { TeacherController } from './controller/teacher.controller';
import { TeacherService } from './service/teacher.service';

@Module({
  imports: [HttpModule],
  controllers: [TeacherController],
  providers: [TeacherService],
})
export class TeacherModule {}
