import { IsString } from 'class-validator';

export class TeacherDto {
  @IsString()
  id: string;

  @IsString()
  name: string;
}
