import { TeacherClassDto } from './teacher-class.dto';

export class TeacherTableDto {
  classes: TeacherClassDto[];
}
