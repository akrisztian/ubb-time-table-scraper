import { IsString } from 'class-validator';
import { StudentClassDto } from 'src/student/dto/student-class.dto';

export class TeacherClassDto extends StudentClassDto {
  @IsString()
  year: string;
}
